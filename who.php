<?php
  require('csv-utils.php');

  $articles = parseCsv('articles.csv');

  $article = getByIdCsv('articles.csv', $_GET['id']);

  session_start();
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>CraieAtor</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="menu.css">
</head>

<body>
<div class="shade">
	<div class="blackboard">
		<div class="form">

            <h2>Qui sommes-nous ?</h2>

            <table>
                <tr>
                    <td><img src="myAvatar 200x200.png" alt="avatar Kelly"></td>
                    <td id="colCenter"><img src="Mohamadi.png" alt="avatar Mohamadi"></td>
                    <td><img src="virginie_avatar_200x200.png" alt="avatar Virginie"></td>
                </tr>

                <tr id="noms">
                    <td>Kelly</td>
                    <td>Mohamadi</td>
                    <td>Virginie</td>
                </tr>

                <tr>
                    <td>CraieAtrice</td>
                    <td>CraieAteur</td>
                    <td>CraieAtrice</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="area"></div>
<nav class="main-menu">
  <ul>
    <?php foreach($articles as $art): ?>
      <li>
        <a href="page.php?id=<?= $art['id'] ?>">
        <i class="fa fa-magic fa-2x"></i>
        <?= $art['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

<ul class="logout">
  <li>
    <a href="create_article.php">
    <i class="fa fa-pencil-square-o fa-2x"></i>
      <span class="nav-text">
        Créer un article
      </span>
    </a>
  </li>
    
    <li>
      <a href="admin.php">
      <i class="fa fa-sitemap fa-2x"></i>
        <span class="nav-text">
          Panel admin
        </span>
      </a>
    </li>
    
    <li>
      <i class="fa fa-power-off fa-2x"></i>
        <span class="nav-text">
          <?php if(isset($_SESSION['id'])) : ?>
              <a href="logout.php">
              <?php echo "Deconnexion";?>
              </a>
            <?php else : ?>
              <a href="login.php">
              <?php echo "Connexion";?>
              </a>
            <?php endif; ?>
        </span>
    </li>
  
  <li>
    <a href="index.php">
    <i class="fa fa-home fa-2x"></i>
      <span class="nav-text">
        Accueil
      </span>
    </a>
  </li>
    
    <li>
      <a href="who.php">
      <i class="fa fa-user fa-2x"></i>
        <span class="nav-text">
          Qui sommes-nous ?
        </span>
      </a>
    </li>
</ul>
</nav>
</body>
</html>