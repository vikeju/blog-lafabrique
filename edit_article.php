<?php
    require('csv-utils.php');

    $articles = parseCsv('articles.csv');

    $article = getByIdCsv('articles.csv', $_GET['id']);

    if($_POST) {
      $article_post = getByIdCsv('articles.csv', $_POST['id']);
        $monArticle = [
            'id' => $_POST['id'],
            'title' => $_POST['title'],
            'author' => $_POST['author'],
            'content' => $_POST['content'],
            'date' => $article_post['date']
        ];

        $success = editCsvRow('articles.csv', $monArticle);
    
        header('Location: admin.php');
    }

    session_start();
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Éditer un article</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="menu.css">

  <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
</head>

<body>
  <div class="shade">
	<div class="blackboard">
		<div class="form">
            <h2>Éditer un article</h2>

            <form id="create_form" action="edit_article.php" method="POST">
                <div>
                    <input type="hidden" id="id" name="id" value="<?= $article['id'] ?>">
                </div>
            
            <div>
                <label for="title">Titre : </label>
                <input type="text" id="title" name="title" value="<?= $article['title'] ?>">
            </div><br><br>

            <div>
                <label for="author">Auteur : </label>
                <input type="text" id="author" name="author" value="<?= $article['author'] ?>">
            </div><br><br>

            <div>
                <label for="content">Contenu : </label><br><br>
                  <div id="toolEd">
                      <div id="toolbar"></div>
                      <div id="editor"></div>
                  </div><br><br>

                <textarea id="content" name="content"><?= $article['content'] ?></textarea>
            </div>
            
            <button onclick="sendForm()">Éditer</button>
            </form>
        </div>
    </div>
</div>

<div class="area"></div>
<nav class="main-menu">
  <ul>
    <?php foreach($articles as $art): ?>
      <li>
        <a href="page.php?id=<?= $art['id'] ?>">
        <i class="fa fa-magic fa-2x"></i>
        <?= $art['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

  <ul class="logout">
    <li>
      <a href="create_article.php">
      <i class="fa fa-pencil-square-o fa-2x"></i>
        <span class="nav-text">
          Créer un article
        </span>
      </a>
    </li>
    
    <li>
      <a href="admin.php">
      <i class="fa fa-sitemap fa-2x"></i>
        <span class="nav-text">
          Panel admin
        </span>
      </a>
    </li>
    
    <li>
      <i class="fa fa-power-off fa-2x"></i>
        <span class="nav-text">
          <?php if(isset($_SESSION['id'])) : ?>
              <a href="logout.php">
              <?php echo "Deconnexion";?>
              </a>
            <?php else : ?>
              <a href="login.php">
              <?php echo "Connexion";?>
              </a>
            <?php endif; ?>
        </span>
    </li>

    <li>
      <a href="index.php">
      <i class="fa fa-home fa-2x"></i>
        <span class="nav-text">
          Accueil
        </span>
      </a>
    </li>
    
    <li>
      <a href="who.php">
      <i class="fa fa-user fa-2x"></i>
        <span class="nav-text">
          Qui sommes-nous ?
        </span>
      </a>
    </li>
  </ul>
</nav>

<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script src="main.js"></script>

<script>
  function unescapeHTML(content) {
    return content.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
  }
    window.onload = function(){
      var editor = document.getElementsByClassName('ql-editor');
      editor[0].innerHTML = unescapeHTML(document.getElementById('content').innerHTML);
    }
</script>
</body>
</html>