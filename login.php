<?php
    require('csv-utils.php');

    $articles = parseCsv('articles.csv');

    $users = parseCsv('users.csv');

    if($_POST) {
        $monAuthent = [
            'name' => $_POST['name'],
            'password' => $_POST['password']
        ];

        foreach($users as $user){
            if($_POST['name'] === $user['name'] && $_POST['password'] === $user['password']){
                session_start();
                $_SESSION['id'] = $user['id'];
                header("Location: admin.php");
                return;
            }
        }

        header("Location: login.php");
    }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Authentification</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="menu.css">
</head>

<body>
  <div class="shade">
	<div class="blackboard">
		<div class="form">
            <h2>Authentification</h2>
            <form action="login.php" method="POST">
            <div>
                <label for="name">Nom : </label>
                <input type="text" id="name" name="name">
            </div>
            <div>
                <label for="password">Mot de passe : </label>
                <input type="password" id="password" name="password">
            </div>
    
            <input type="submit" value="Se connecter">
            </form>
        </div>
    </div>
</div>

<div class="area"></div>
<nav class="main-menu">
  <ul>
    <?php foreach($articles as $art): ?>
      <li>
        <a href="page.php?id=<?= $art['id'] ?>">
        <i class="fa fa-magic fa-2x"></i>
        <?= $art['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>

  <ul class="logout">
    <li>
      <a href="create_article.php">
      <i class="fa fa-pencil-square-o fa-2x"></i>
        <span class="nav-text">
          Créer un article
        </span>
      </a>
    </li>
    
    <li>
      <a href="admin.php">
      <i class="fa fa-sitemap fa-2x"></i>
        <span class="nav-text">
          Panel admin
        </span>
      </a>
    </li>

    <li>
      <a href="index.php">
      <i class="fa fa-home fa-2x"></i>
        <span class="nav-text">
          Accueil
        </span>
      </a>
    </li>
    
    <li>
      <a href="who.php">
      <i class="fa fa-user fa-2x"></i>
        <span class="nav-text">
          Qui sommes-nous ?
        </span>
      </a>
    </li>
  </ul>
</nav>
</body>
</html>