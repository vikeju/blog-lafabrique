<?php

function parseCsv($fileName) {
  $content = [];
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    $numCol = count($keys);
    // First column must be id
    if($keys[0] !== 'id') {
      echo "Erreur dans le fichier $fileName, la première colonne n'est pas 'id'";
      return FALSE;
    }

    while (($data = fgetcsv($handle)) !== FALSE) {
      $row = [];
      for ($c=0; $c < $numCol; $c++) {
        if($c === 0 && $data[$c] === "" || is_integer((int)$data[$c]) === FALSE) {
          echo "<br>Erreur dans le fichier $fileName, 'id' invalide sur la ligne";
          print_r($data);
        }
        $row[$keys[$c]] = $data[$c];
      }
      $content[] = $row;
    }
    fclose($handle);
  }
  return $content;
}

function getCsvContentKey($content, $id) {
  return array_search($id, array_column($content, 'id'));
}

function getByIdCsv($fileName, $id) {
  $content = parseCsv($fileName);
  $key = getCsvContentKey($content, $id);
  if($key !== FALSE) {
    return $content[$key];
  }
  return FALSE;
}

/* Note : first column must be id */
function getNextIdCsv($fileName) {
  $nextId = -1;
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    if($keys[0] === 'id') {
      while (($data = fgetcsv($handle)) !== FALSE) {
        if($data[0] >= $nextId) {
          $nextId = $data[0] + 1;
        }
      }
    }
  }
  return $nextId;
}

function buildCsvRow($keys, $row) {
  $csvRow = [];
  foreach ($keys as $index => $key) {
    if(array_key_exists($key, $row)) {
      $value = $row[$key];
    } else {
      $value = "";
    }
    $csvRow[$index] = $value;
  }
  return $csvRow;
}

function addToCsv($fileName, $row) {
  $id = getNextIdCsv($fileName);
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    fclose($handle);
  }
  $handle = fopen($fileName, 'a');
  if ($handle !== FALSE && $id !== -1) {
    $row['id'] = $id;
    $csvRow = buildCsvRow($keys, $row);
    fputcsv($handle, $csvRow);
    fclose($handle);
    return TRUE;
  }
  return FALSE;
}

function writeContentToCsv($fileName, $content) {
  $handle = fopen($fileName, 'w');
  if($handle !== FALSE && count($content) > 0) {
    $keys = array_keys($content[0]);
    fputcsv($handle, $keys);
    foreach ($content as $row) {
      $csvRow = buildCsvRow($keys, $row);
      fputcsv($handle, $csvRow);
    }
    fclose($handle);
    return TRUE;
  }
  return FALSE;
}

function removeFromCsv($fileName, $id) {
  $content = parseCsv($fileName);
  $key = getCsvContentKey($content, $id);
  if($key !== FALSE) {
    array_splice($content, $key, 1);
    return writeContentToCsv($fileName, $content);
  }
  return FALSE;
}

function editCsvRow($fileName, $row) {
  $content = parseCsv($fileName);
  $key = getCsvContentKey($content, $row['id']);
  if($key !== FALSE) {
    $content[$key] = $row;
    return writeContentToCsv($fileName, $content);
  }
  return FALSE;
}